package com.roldan.a20230303_danielroldan_nycschools

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class SchoolsApplication: Application() {
}