package com.roldan.a20230303_danielroldan_nycschools.core.di

import com.roldan.a20230303_danielroldan_nycschools.features.details.services.SatApi
import com.roldan.a20230303_danielroldan_nycschools.features.schools.services.SchoolApi
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.create
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
object ApiModule {

    @Singleton
    @Provides
    fun providesSatApi(retrofit: Retrofit): SatApi = retrofit.create(SatApi::class.java)

    @Singleton
    @Provides
    fun providesSchoolApi(retrofit: Retrofit): SchoolApi = retrofit.create(SchoolApi::class.java)

    @Singleton
    @Provides
    fun providesRetrofit(okhttpClient: OkHttpClient): Retrofit = Retrofit.Builder().client(okhttpClient).baseUrl("https://data.cityofnewyork.us/resource/").addConverterFactory(GsonConverterFactory.create()).build()

    @Provides
    @Singleton
    fun providesOkHttpClient(): OkHttpClient = OkHttpClient.Builder().build()
}