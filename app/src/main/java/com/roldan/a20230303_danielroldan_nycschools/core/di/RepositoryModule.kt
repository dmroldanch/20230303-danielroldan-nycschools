package com.roldan.a20230303_danielroldan_nycschools.core.di

import com.roldan.a20230303_danielroldan_nycschools.features.details.services.SatApi
import com.roldan.a20230303_danielroldan_nycschools.features.details.services.SatRepository
import com.roldan.a20230303_danielroldan_nycschools.features.details.services.SatRepositoryImpl
import com.roldan.a20230303_danielroldan_nycschools.features.schools.services.SchoolApi
import com.roldan.a20230303_danielroldan_nycschools.features.schools.services.SchoolRepository
import com.roldan.a20230303_danielroldan_nycschools.features.schools.services.SchoolRepositoryImpl
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
object RepositoryModule {
    @Singleton
    @Provides
    fun providesSchoolRepository(
        schoolApi: SchoolApi
    ) : SchoolRepository = SchoolRepositoryImpl(schoolApi)

    @Singleton
    @Provides
    fun providesSatRepository(
        satApi: SatApi
    ) : SatRepository = SatRepositoryImpl(satApi)
}