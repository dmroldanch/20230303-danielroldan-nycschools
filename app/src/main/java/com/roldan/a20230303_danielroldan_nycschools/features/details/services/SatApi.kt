package com.roldan.a20230303_danielroldan_nycschools.features.details.services

import retrofit2.Response
import retrofit2.http.GET

interface SatApi {
    @GET("f9bf-2cp4.json")
    suspend fun getSatScores(): Response<List<SatResponse>>
}