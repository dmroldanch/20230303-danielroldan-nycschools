package com.roldan.a20230303_danielroldan_nycschools.features.schools.services

import com.roldan.a20230303_danielroldan_nycschools.features.schools.models.School
import com.roldan.a20230303_danielroldan_nycschools.features.schools.services.SchoolResponse.Companion.toDomain
import javax.inject.Inject

class SchoolRepositoryImpl @Inject constructor(
    private val schoolApi: SchoolApi
): SchoolRepository {
    override suspend fun getSchoolList(): List<School> {
        val response = schoolApi.getSchoolList()
        return if(response.isSuccessful){
            response.body()?.map { it.toDomain() }?: emptyList()
        } else {
            emptyList()
        }
    }
}