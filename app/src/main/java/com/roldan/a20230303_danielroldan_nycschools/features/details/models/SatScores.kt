package com.roldan.a20230303_danielroldan_nycschools.features.details.models

data class SatScores(
    val id: String,
    val avgMath: String,
    val avgReading: String,
    val avgWriting: String
)