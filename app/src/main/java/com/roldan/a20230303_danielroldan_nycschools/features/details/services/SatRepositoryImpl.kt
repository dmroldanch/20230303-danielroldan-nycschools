package com.roldan.a20230303_danielroldan_nycschools.features.details.services

import com.roldan.a20230303_danielroldan_nycschools.features.details.models.SatScores
import com.roldan.a20230303_danielroldan_nycschools.features.details.services.SatResponse.Companion.toDomain
import javax.inject.Inject

class SatRepositoryImpl @Inject constructor(
    private val satApi: SatApi
): SatRepository {
    override suspend fun getSatScores(): List<SatScores> {
        val response = satApi.getSatScores()
        return if(response.isSuccessful){
            response.body()?.map { it.toDomain() }?: emptyList()
        } else {
            emptyList()
        }
    }
}