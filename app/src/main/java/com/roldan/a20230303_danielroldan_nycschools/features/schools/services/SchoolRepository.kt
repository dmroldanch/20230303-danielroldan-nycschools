package com.roldan.a20230303_danielroldan_nycschools.features.schools.services

import com.roldan.a20230303_danielroldan_nycschools.features.schools.models.School

interface SchoolRepository {
    suspend fun getSchoolList(): List<School>
}