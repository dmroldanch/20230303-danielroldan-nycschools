package com.roldan.a20230303_danielroldan_nycschools.features.details.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.roldan.a20230303_danielroldan_nycschools.features.details.models.SatScores
import com.roldan.a20230303_danielroldan_nycschools.features.details.services.SatRepository
import com.roldan.a20230303_danielroldan_nycschools.features.schools.models.School
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class SchoolDetailsViewModel @Inject constructor(
    private val satRepository: SatRepository
): ViewModel() {

    private val _score = MutableLiveData<SatScores?>()
    val score: LiveData<SatScores?> get() = _score

    fun getSchoolSatScores(school: School){
        viewModelScope.launch(Dispatchers.IO) {
            val scores = satRepository.getSatScores()
            val schoolScore = scores.find { it.id == school.id }
            _score.postValue(schoolScore)
        }
    }
}