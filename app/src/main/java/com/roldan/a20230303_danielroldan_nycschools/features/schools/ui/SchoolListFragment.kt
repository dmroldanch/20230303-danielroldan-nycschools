package com.roldan.a20230303_danielroldan_nycschools.features.schools.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.roldan.a20230303_danielroldan_nycschools.databinding.SchoolsListFragmentBinding
import com.roldan.a20230303_danielroldan_nycschools.features.schools.models.School
import com.roldan.a20230303_danielroldan_nycschools.features.schools.viewmodels.SchoolViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class SchoolListFragment: Fragment() {

    lateinit var binding: SchoolsListFragmentBinding
    private val schoolViewModel by viewModels<SchoolViewModel>()
    private val adapter = SchoolsAdapter(::onSchoolClicked)

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = SchoolsListFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        schoolViewModel.getSchoolList()

        binding.schoolsList.adapter = adapter

        schoolViewModel.list.observe(viewLifecycleOwner){
            adapter.submitList(it)
        }
    }

    private fun onSchoolClicked(school: School) {
        findNavController().navigate(SchoolListFragmentDirections.actionSchoolListFragmentToSchoolDetailsFragment(school))
    }


}