package com.roldan.a20230303_danielroldan_nycschools.features.details.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.navArgs
import com.roldan.a20230303_danielroldan_nycschools.databinding.SchoolDetailsFragmentBinding
import com.roldan.a20230303_danielroldan_nycschools.features.details.viewmodels.SchoolDetailsViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class SchoolDetailsFragment: Fragment() {
    private val detailsViewModel by viewModels<SchoolDetailsViewModel>()
    lateinit var binding: SchoolDetailsFragmentBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = SchoolDetailsFragmentBinding.inflate(layoutInflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val school = navArgs<SchoolDetailsFragmentArgs>().value.schoolClicked
        detailsViewModel.getSchoolSatScores(school)
        detailsViewModel.score.observe(viewLifecycleOwner){
            with(binding){
                avgMath.text = it?.avgMath ?: "-"
                avgReading.text = it?.avgReading ?: "-"
                avgWriting.text = it?.avgWriting ?: "-"

                activity?.actionBar?.title = school.name
                schoolNameDetails.text = school.name
                schoolDescription.text = school.description

            }
        }
    }
}