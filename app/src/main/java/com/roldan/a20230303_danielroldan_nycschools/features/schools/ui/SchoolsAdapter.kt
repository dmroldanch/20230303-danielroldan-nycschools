package com.roldan.a20230303_danielroldan_nycschools.features.schools.ui

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import com.roldan.a20230303_danielroldan_nycschools.databinding.SchoolCellBinding
import com.roldan.a20230303_danielroldan_nycschools.features.schools.models.School

class SchoolsAdapter(
    private val onSchoolDetailsClick: (School) -> Unit
): ListAdapter<School, SchoolsAdapter.SchoolVH>(SchoolDiff()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SchoolVH {
        val layoutInflater = LayoutInflater.from(parent.context)
        val vh = SchoolVH(
            SchoolCellBinding.inflate(layoutInflater, parent, false)
        )
        return vh
    }

    override fun onBindViewHolder(holder: SchoolVH, position: Int) {
        holder.bindSchool(getItem(position))
    }

    inner class SchoolVH(private val binding: SchoolCellBinding): ViewHolder(binding.root){
        fun bindSchool(school: School){
            with(binding){
                schoolName.text = school.name
                scholLocation.text = school.location
                detailsBtn.setOnClickListener { onSchoolDetailsClick(school) }
            }
        }
    }

    class SchoolDiff: DiffUtil.ItemCallback<School>(){
        override fun areItemsTheSame(oldItem: School, newItem: School): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: School, newItem: School): Boolean {
            return oldItem == newItem
        }
    }
}