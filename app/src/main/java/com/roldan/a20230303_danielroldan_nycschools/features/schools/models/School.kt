package com.roldan.a20230303_danielroldan_nycschools.features.schools.models

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class School(
    val id: String,
    val name: String,
    val location: String,
    val description: String

): Parcelable