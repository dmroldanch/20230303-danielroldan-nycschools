package com.roldan.a20230303_danielroldan_nycschools.features.schools.services

import retrofit2.Response
import retrofit2.http.GET

interface SchoolApi {
    @GET("s3k6-pzi2.json")
    suspend fun getSchoolList() : Response<List<SchoolResponse>>
}