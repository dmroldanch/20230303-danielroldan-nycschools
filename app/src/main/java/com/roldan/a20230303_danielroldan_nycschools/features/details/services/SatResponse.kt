package com.roldan.a20230303_danielroldan_nycschools.features.details.services


import com.google.gson.annotations.SerializedName
import com.roldan.a20230303_danielroldan_nycschools.features.details.models.SatScores

data class SatResponse(
    @SerializedName("dbn")
    val dbn: String,
    @SerializedName("num_of_sat_test_takers")
    val numOfSatTestTakers: String,
    @SerializedName("sat_critical_reading_avg_score")
    val satCriticalReadingAvgScore: String,
    @SerializedName("sat_math_avg_score")
    val satMathAvgScore: String,
    @SerializedName("sat_writing_avg_score")
    val satWritingAvgScore: String,
    @SerializedName("school_name")
    val schoolName: String
){
    companion object {
        fun SatResponse.toDomain() = SatScores(
            id = dbn,
            avgMath = satMathAvgScore,
            avgReading = satCriticalReadingAvgScore,
            avgWriting = satWritingAvgScore
        )
    }
}