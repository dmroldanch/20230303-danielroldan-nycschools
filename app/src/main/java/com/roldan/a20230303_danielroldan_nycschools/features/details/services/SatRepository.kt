package com.roldan.a20230303_danielroldan_nycschools.features.details.services

import com.roldan.a20230303_danielroldan_nycschools.features.details.models.SatScores

interface SatRepository {
    suspend fun getSatScores(): List<SatScores>
}