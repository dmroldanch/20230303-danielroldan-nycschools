package com.roldan.a20230303_danielroldan_nycschools.features.schools.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.roldan.a20230303_danielroldan_nycschools.features.schools.models.School
import com.roldan.a20230303_danielroldan_nycschools.features.schools.services.SchoolRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject


@HiltViewModel
class SchoolViewModel @Inject constructor(private val schoolRepository: SchoolRepository): ViewModel() {

    private val _list = MutableLiveData<List<School>>()
    val list: LiveData<List<School>> get() = _list

    fun getSchoolList(){
        viewModelScope.launch(Dispatchers.IO) {
            val list = schoolRepository.getSchoolList()
            _list.postValue(list)
        }
    }

}